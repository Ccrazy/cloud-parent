package com.bitbucket.job.registry.zk;

import com.bitbucket.job.registry.DistributeRegistryCenter;

import java.util.List;

/**
 * ZkDistributeRegCenter
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
public class ZkDistributeRegCenter implements DistributeRegistryCenter {
    @Override
    public String getRemote(String key) {
        return null;
    }

    @Override
    public List<String> getChildrenKeys(String key) {
        return null;
    }

    @Override
    public int getNumChildren(String key) {
        return 0;
    }

    @Override
    public void persistEphemeral(String key, String value) {

    }

    @Override
    public String persistSequential(String key, String value) {
        return null;
    }

    @Override
    public void persistEphemeralSequential(String key) {

    }

    @Override
    public void addCacheData(String cachePath) {

    }

    @Override
    public void evictCacheData(String cachePath) {

    }

    @Override
    public Object getRawCache(String cachePath) {
        return null;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void restart() {

    }

    @Override
    public String getState() {
        return null;
    }

    @Override
    public String get(String key) {
        return null;
    }

    @Override
    public boolean save(String key, String value) {
        return false;
    }

    @Override
    public boolean update(String key, String value) {
        return false;
    }

    @Override
    public boolean delete(String key) {
        return false;
    }

    @Override
    public Object getClient() {
        return null;
    }
}
