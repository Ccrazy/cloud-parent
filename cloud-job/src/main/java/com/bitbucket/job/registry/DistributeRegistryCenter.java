package com.bitbucket.job.registry;

import java.util.List;

/**
 * DistributeRegistryCenter
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
public interface DistributeRegistryCenter extends RegistryCenter {

    String getRemote(String key);


    List<String> getChildrenKeys(String key);


    int getNumChildren(String key);


    void persistEphemeral(String key, String value);

    String persistSequential(String key, String value);


    void persistEphemeralSequential(String key);


    void addCacheData(String cachePath);


    void evictCacheData(String cachePath);


    Object getRawCache(String cachePath);

}
