package com.bitbucket.job.registry;

/**
 * RegistryCenter
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
public interface RegistryCenter {

    /**
     * 启动
     */
    void start();
    /**
     * 停止
     */
    void stop();

    /**
     * 重启注册中心
     */
    void restart();

    /**
     * 获取注册中心状态
     *
     * @return
     */
    String getState();
    /**
     * 获取数据
     *
     * @param key
     * @return
     */
    String get(String key);

    /**
     * 保存
     *
     * @param key
     * @param value
     * @return
     */
    boolean save(String key, String value);

    /**
     * 更新
     *
     * @param key
     * @param value
     * @return
     */
    boolean update(String key, String value);

    /**
     * 删除
     *
     * @param key
     * @return
     */
    boolean delete(String key);

    /**
     * 获取client
     *
     * @return
     */
    Object getClient();

}
