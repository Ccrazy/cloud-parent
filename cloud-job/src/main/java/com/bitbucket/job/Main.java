package com.bitbucket.job;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Main
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
public class Main {

    public static void main(String[] args) throws Exception {
        RetryPolicy policy = new ExponentialBackoffRetry(1000, 10);
        String connectString = "localhost:10011,localhost:10012,localhost:10013";
        CuratorFramework curator = CuratorFrameworkFactory.builder().retryPolicy(policy).connectString(connectString).build();
        curator.start();

        try {
            String s = curator.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath("/super/c1", "this data".getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            String s = new String(curator.getData().forPath("/super/c1"));
            System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<String> strings = curator.getChildren().forPath("/super");
        System.out.println(strings);

    }
}
