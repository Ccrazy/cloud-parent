package com.baidu.bce.persistence.db.engine;

import com.baidu.bce.persistence.db.controller.ControllerPackage;
import com.baidu.bce.persistence.db.repository.RepositoryPackage;
import com.baidu.bce.persistence.db.service.ServicePackage;
import com.bitbucket.ano.cache.CacheInstance;
import com.bitbucket.ano.cache.CacheType;
import com.bitbucket.ano.cache.EnableCache;
import com.bitbucket.jpa.ano.EnablePersitence;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author yupeng
 */
@SpringBootApplication
@EnablePersitence(basePackageClasses = RepositoryPackage.class)
@ComponentScan(basePackageClasses = ServicePackage.class)
@ComponentScan(basePackageClasses = ControllerPackage.class)
@EnableCache(type = CacheType.EhCache, instance = {
        @CacheInstance(value = "10s", ttl = 10),
        @CacheInstance(value = "20s", ttl = 20)
})
public class Persistence {
    public static void main(String[] args) {
        SpringApplication.run(Persistence.class, args);
    }
}