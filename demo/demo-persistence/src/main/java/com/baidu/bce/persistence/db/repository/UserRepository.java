package com.baidu.bce.persistence.db.repository;

import com.baidu.bce.persistence.db.model.TblUser;
import com.bitbucket.jpa.common.repo.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * UserRepository
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Repository
public interface UserRepository extends BaseRepository<TblUser> {
}
