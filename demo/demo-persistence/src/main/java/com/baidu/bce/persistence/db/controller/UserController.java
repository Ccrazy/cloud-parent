package com.baidu.bce.persistence.db.controller;

import com.baidu.bce.persistence.db.model.TblUser;
import com.baidu.bce.persistence.db.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * UserController
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/find")
    public void find() {
        List<TblUser> all = userService.findAll();
        System.out.println(all);
    }

    @GetMapping("/save")
    public void save() {
        userService.save(new TblUser());
    }


    @GetMapping("/find10")
    public void find10() {
        userService.findCache10();
    }


    @GetMapping("/find20")
    public void find20() {
        userService.findCache20();
    }

}
