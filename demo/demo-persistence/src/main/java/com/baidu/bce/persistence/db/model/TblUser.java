package com.baidu.bce.persistence.db.model;

import com.bitbucket.jpa.common.model.DefaultIdDO;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table
@Entity
@Data
public class TblUser extends DefaultIdDO {
    private String name;
    private Integer status;
}
