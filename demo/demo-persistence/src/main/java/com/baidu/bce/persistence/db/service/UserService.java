package com.baidu.bce.persistence.db.service;

import com.baidu.bce.persistence.db.model.TblUser;
import com.bitbucket.jpa.common.repo.BaseRepository;
import com.bitbucket.jpa.common.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * UserService
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Service
@Slf4j
public class UserService extends BaseService<TblUser> {
    public UserService(BaseRepository<TblUser> repository) {
        super(repository);
    }

    @Cacheable("10s")
    public List<TblUser> findCache10() {
        log.info("in findCache10");
        return this.findAll();
    }
    @Cacheable("20s")
    public List<TblUser> findCache20() {
        log.info("in findCache20");
        return this.findAll();
    }
}
