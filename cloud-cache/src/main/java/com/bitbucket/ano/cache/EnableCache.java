package com.bitbucket.ano.cache;

import com.bitbucket.configuration.cache.CacheConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * EnableCache
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({CacheConfiguration.class})
public @interface EnableCache {

    /**
     * 缓存类型
     *
     * @return
     */
    CacheType type();

    /**
     * 缓存实例
     *
     * @return
     */
    CacheInstance[] instance();

}
