package com.bitbucket.ano.cache;

import lombok.Getter;

/**
 * CacheType
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Getter
public enum CacheType {
    /**
     * 缓存类型
     */
    EhCache, Redis;

}
