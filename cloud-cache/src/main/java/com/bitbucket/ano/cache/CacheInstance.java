package com.bitbucket.ano.cache;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * CacheInstance
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheInstance {

    long ttl() default 60;

    long tti() default 0;

    int maxCapacity() default 1000;

    String value();

}
