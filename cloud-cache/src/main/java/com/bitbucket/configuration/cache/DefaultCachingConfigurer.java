package com.bitbucket.configuration.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;

/**
 * DefaultCachingConfigurerImpl
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DefaultCachingConfigurer implements CachingConfigurer {

    private CacheManager cacheManager;
    private CacheResolver cacheResolver;
    private KeyGenerator keyGenerator;
    private CacheErrorHandler cacheErrorHandler;


    @Override
    public CacheManager cacheManager() {
        return cacheManager;
    }

    @Override
    public CacheResolver cacheResolver() {
        return cacheResolver;
    }

    @Override
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }

    @Override
    public CacheErrorHandler errorHandler() {
        return errorHandler();
    }
}
