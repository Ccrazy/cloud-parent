package com.bitbucket.configuration.cache;

import com.bitbucket.configuration.cache.factory.CachingConfigurerFactoryBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.type.AnnotationMetadata;

/**
 * CacheConfiguration
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Configuration
@EnableCaching
public class CacheConfiguration implements ImportAware {

    private AnnotationMetadata annotationMetadata;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        this.annotationMetadata = importMetadata;
    }


    @Bean
    public CachingConfigurerFactoryBean cachingConfigurerFactoryBean() {
        return new CachingConfigurerFactoryBean(annotationMetadata);
    }
}
