package com.bitbucket.configuration.cache;

import lombok.Data;

/**
 * CacheAttr
 *
 * 参照 @CacheInstance , @EnableCache
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Data
public class CacheAttr {

    long ttl;

    long tti;

    int maxCapacity;

    String name;
}
