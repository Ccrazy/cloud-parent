package com.bitbucket.configuration.cache.factory;

import com.bitbucket.ano.cache.CacheType;
import com.bitbucket.configuration.cache.CacheAttr;
import com.google.common.collect.Maps;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.time.Duration;
import java.util.Map;
import java.util.Set;

/**
 * CacheManageConstructor
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
public class CacheManageConstructor {

    public static org.springframework.cache.CacheManager createCacheManager(CacheType type,
                                                                            Set<CacheAttr> caches,
                                                                            BeanFactory beanFactory) {
        switch (type) {
            case EhCache:
                CacheManager cacheManager = CacheManager.create();
                EhCacheCacheManager ehCacheCacheManager = new EhCacheCacheManager(cacheManager);
                if (caches != null) {
                    caches.forEach(c -> {
                        Cache cache = new Cache(
                                c.getName(),
                                c.getMaxCapacity(),
                                false,
                                false,
                                c.getTtl(),
                                c.getTti()
                        );
                        cacheManager.addCache(cache);
                    });
                }
                return ehCacheCacheManager;
            case Redis:
                RedisConnectionFactory redisConnectionFactory = beanFactory
                        .getBean(RedisConnectionFactory.class);
                Map<String, RedisCacheConfiguration> maps = Maps.newHashMap();
                caches.forEach(cacheAttr -> {
                    Duration duration = Duration
                            .ofSeconds(cacheAttr.getTtl());
                    maps.put(cacheAttr.getName(), RedisCacheConfiguration
                            .defaultCacheConfig()
                            .entryTtl(duration));
                });
                return RedisCacheManager
                        .builder(redisConnectionFactory)
                        .withInitialCacheConfigurations(maps)
                        .build();
            default:
                break;
        }
        return null;
    }

}
