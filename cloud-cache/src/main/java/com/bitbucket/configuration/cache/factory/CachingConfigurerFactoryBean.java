package com.bitbucket.configuration.cache.factory;

import com.bitbucket.ano.cache.CacheType;
import com.bitbucket.ano.cache.EnableCache;
import com.bitbucket.configuration.cache.CacheAttr;
import com.bitbucket.configuration.cache.DefaultCachingConfigurer;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.SimpleCacheErrorHandler;
import org.springframework.cache.interceptor.SimpleCacheResolver;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Set;

/**
 * CacheManageFactory
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */

public class CachingConfigurerFactoryBean extends AbstractFactoryBean<DefaultCachingConfigurer> {

    private AnnotationAttributes attributes;

    @Override
    public Class<?> getObjectType() {
        return CachingConfigurerFactoryBean.class;
    }

    public CachingConfigurerFactoryBean(AnnotationMetadata annotationMetadata) {
        AnnotationAttributes annotationAttributes = AnnotationAttributes.fromMap(
                annotationMetadata.getAnnotationAttributes(EnableCache.class.getName(), false));
        this.attributes = annotationAttributes;
    }

    private Set<CacheAttr> cachAttrs() {
        AnnotationAttributes[] caches = attributes.getAnnotationArray("instance");
        Set<CacheAttr> cacheAttrs = Sets.newHashSet();
        for (AnnotationAttributes instance : caches) {
            CacheAttr cacheAttr = new CacheAttr();
            cacheAttr.setMaxCapacity(instance.getNumber("maxCapacity"));
            cacheAttr.setName(instance.getString("value"));
            cacheAttr.setTtl(instance.getNumber("ttl"));
            cacheAttr.setTti(instance.getNumber("tti"));
            cacheAttrs.add(cacheAttr);
        }
        return cacheAttrs;
    }

    @Override
    protected DefaultCachingConfigurer createInstance() {
        DefaultCachingConfigurer cachingConfigurer = new DefaultCachingConfigurer();
        CacheType type = (CacheType) attributes.get("type");
        Set<CacheAttr> cacheAttrs = cachAttrs();
        CacheManager cacheManager = CacheManageConstructor.createCacheManager(type, cacheAttrs, getBeanFactory());
        cachingConfigurer.setCacheManager(cacheManager);
        cachingConfigurer.setKeyGenerator(new SimpleKeyGenerator());
        cachingConfigurer.setCacheResolver(new SimpleCacheResolver(cacheManager));
        cachingConfigurer.setCacheErrorHandler(new SimpleCacheErrorHandler());
        return cachingConfigurer;
    }


}
