package com.bitbucket.jpa.configuration;

import com.bitbucket.jpa.ano.EnablePersitence;
import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;
import org.springframework.data.repository.config.RepositoryBeanDefinitionRegistrarSupport;
import org.springframework.data.repository.config.RepositoryConfigurationExtension;

import java.lang.annotation.Annotation;

/**
 * JpaRepositoryBeanDefinitionRegistrar
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
public class JpaRepositoryBeanDefinitionRegistrar extends RepositoryBeanDefinitionRegistrarSupport {
    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return EnablePersitence.class;
    }
    @Override
    protected RepositoryConfigurationExtension getExtension() {
        return new JpaRepositoryConfigExtension();
    }
}
