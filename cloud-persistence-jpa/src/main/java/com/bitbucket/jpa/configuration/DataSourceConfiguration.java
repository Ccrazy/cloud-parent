package com.bitbucket.jpa.configuration;

import com.bitbucket.jpa.config.DataSourceConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * DataSourceAutoConfiguration
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Configuration
public class DataSourceConfiguration {

    @Autowired
    private DataSourceConfig dataSourceConfig;

    @Bean
    @ConditionalOnMissingBean
    @Primary
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setMaximumPoolSize(500);
        if (dataSourceConfig.getMaxPoolSize() != null) {
            dataSource.setMaximumPoolSize(dataSourceConfig.getMaxPoolSize());
        }
        dataSource.setJdbcUrl(dataSourceConfig.getJdbcUrl());
        dataSource.setDriverClassName(dataSourceConfig.getDriverName());
        dataSource.setUsername(dataSourceConfig.getUsername());
        dataSource.setPassword(dataSourceConfig.getPassword());
        dataSource.setMaxLifetime(MINUTES.toMillis(1));
        return dataSource;
    }

}
