package com.bitbucket.jpa.configuration;

import com.bitbucket.jpa.config.JpaConfig;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * JpaConfiguration
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class JpaConfiguration {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JpaConfig jpaConfig;


    @Bean
    @ConditionalOnMissingBean
    public EntityManagerFactory entityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        Properties jpaProperties = new Properties();
        jpaProperties.setProperty(AvailableSettings.SHOW_SQL, jpaConfig.getShowSql());
        jpaProperties.setProperty(AvailableSettings.HBM2DDL_AUTO, jpaConfig.getHbm2ddl());
        jpaProperties.setProperty(AvailableSettings.FORMAT_SQL, jpaConfig.getFormatSql());
        jpaProperties.setProperty(AvailableSettings.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new
                LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
        localContainerEntityManagerFactoryBean.setPackagesToScan(jpaConfig.getModelPackage());
        localContainerEntityManagerFactoryBean.setDataSource(dataSource);
        localContainerEntityManagerFactoryBean.setJpaProperties(jpaProperties);
        localContainerEntityManagerFactoryBean.afterPropertiesSet();
        return localContainerEntityManagerFactoryBean.getObject();
    }

    @Bean
    @ConditionalOnMissingBean
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return new JpaTransactionManager(entityManagerFactory());
    }
}
