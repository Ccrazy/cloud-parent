package com.bitbucket.jpa.common.model;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * DefaultIdDO
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Data
@MappedSuperclass
public class DefaultIdDO implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
