package com.bitbucket.jpa.common.service;

import com.bitbucket.jpa.common.repo.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import com.querydsl.core.types.Predicate;
import java.util.List;

/**
 * BaseService
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
public abstract class BaseService<T> {

    private BaseRepository<T> repository;

    public BaseService(BaseRepository<T> repository) {
        this.repository = repository;
    }

    public List<T> findAll() {
        return repository.findAll();
    }

    public List<T> findAll(Sort sort) {
        return repository.findAll();
    }

    public Page<T> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Iterable<T> findAll(Predicate predicate) {
        return repository.findAll(predicate);
    }

    public Iterable<T> findAll(Iterable<Long> ids) {
        return repository.findAllById(ids);
    }

    public Page<T> findAll(Predicate predicate, Pageable pageable) {
        return repository.findAll(predicate, pageable);
    }

    public T findOne(Long id) {
        return repository.findById(id).get();
    }

    public T findOne(Predicate predicate) {
        return repository.findOne(predicate).get();
    }

    public boolean exists(Long id) {
        return repository.existsById(id);
    }

    public long count() {
        return repository.count();
    }

    public long count(Predicate predicate) {
        return repository.count(predicate);
    }

    @Transactional
    public <S extends T> S save(final S entity) {
        return (S) this.repository.save(entity);
    }

    @Transactional
    public <S extends T> List<S> save(final Iterable<S> entities) {
        return this.repository.saveAll(entities);
    }

    @Transactional
    public T saveAndFlush(final T entity) {
        return (T) this.repository.saveAndFlush(entity);
    }

    @Transactional
    public void flush() {
        this.repository.flush();
    }

    @Transactional
    public void delete(final Long id) {
        this.repository.deleteById(id);
    }

    @Transactional
    public void delete(final T entity) {
        this.repository.delete(entity);
    }

    @Transactional
    public void delete(final Iterable<? extends T> entities) {
        this.repository.deleteAll(entities);
    }

    @Transactional
    public void deleteInBatch(final Iterable<T> entities) {
        this.repository.deleteInBatch((Iterable) entities);
    }

    @Transactional
    public void deleteAllInBatch() {
        this.repository.deleteAllInBatch();
    }

    @Transactional
    public void deleteAll() {
        this.repository.deleteAll();
    }

}
