package com.bitbucket.jpa.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * JpaConfig
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@ConfigurationProperties(prefix = "jpa")
@Configuration
@Data
public class JpaConfig {
    private String modelPackage;
    private String showSql = "false";
    private String hbm2ddl = "none";
    private String formatSql = "false";

}
