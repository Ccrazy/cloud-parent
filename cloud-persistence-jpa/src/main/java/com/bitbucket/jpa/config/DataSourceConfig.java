package com.bitbucket.jpa.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * DataSourceConfig
 *
 * @author yupeng10@baidu.com
 * @since 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "data-source")
@Data
public class DataSourceConfig {
    private String jdbcUrl;
    private String username;
    private String password;
    private String driverName;
    private Integer maxPoolSize;
}
